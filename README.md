# meme-language

Project for NLP classes ("Analysis of meme language")

```bash
docker build ${PWD} --tag meme2vec:nlp
docker run --rm -it -v ${PWD}:/project -p 8888:8888 meme2vec:nlp
```
