"""
Functions and classes for handling meme dataset
"""
from keras.preprocessing import sequence as ks_seq
import numpy as np
import pandas as pd
from sklearn import model_selection as sk_ms


class Dataset:
    def __init__(self, meme_texts_file, word_embeddings_file, test_split,
                 top_n=None, max_text_emb_len=16):
        self._meme_texts = None
        self._word_mappings = None
        self._word_embeddings = None
        self._test_split = test_split
        self._top_n = top_n
        self._max_text_emb_len = max_text_emb_len

        self._X = None
        self._Y = None
        self._nb_classes = None

        self._X_train = None
        self._X_test = None
        self._Y_train = None
        self._Y_test = None

        self._load_word_embeddings(word_embeddings_file)
        self._load_meme_texts(meme_texts_file)
        self._embed_meme_texts()
        self._make_train_test()

    def _load_meme_texts(self, filepath):
        df = pd.read_json(filepath)
        df.dropna(inplace=True)

        if self._top_n:
            top_cats = (df.groupby('template')
                         .count()
                         .sort_values('lemmatized_txt', ascending=False)
                         [:self._top_n].index)
            df = df[df.template.isin(top_cats)].reset_index(drop=True)

        df.template = df.template.astype('category')

        self._meme_texts = df

    def _load_word_embeddings(self, filepath):
        embs = np.load(filepath).item()
        self._word_mappings = embs['mappings']

        padding_emb = np.zeros(embs['embeddings'].shape[1])
        self._word_embeddings = np.vstack((padding_emb, embs['embeddings']))

    def _embed_meme_texts(self):
        sequences = self._meme_texts.lemmatized_txt.map(
            lambda txt: [self._word_mappings.get(word, -1) + 1 for word in txt]
        )

        padded_seqs = ks_seq.pad_sequences(
            sequences, maxlen=self._max_text_emb_len
        )
        self._X = padded_seqs

    def _make_train_test(self):
        self._Y = self._meme_texts.template.cat.codes
        self._nb_classes = self._meme_texts.template.unique().shape[0]

        tts = sk_ms.train_test_split(
            self._X, self._Y,
            test_size=self._test_split,
            random_state=42
        )
        self._X_train = tts[0]
        self._X_test = tts[1]
        self._Y_train = tts[2]
        self._Y_test = tts[3]

    @property
    def X_train(self):
        return self._X_train

    @property
    def Y_train(self):
        return self._Y_train

    @property
    def X_test(self):
        return self._X_test

    @property
    def Y_test(self):
        return self._Y_test

    @property
    def word_embeddings(self):
        return self._word_embeddings

    @property
    def nb_classes(self):
        return self._nb_classes

