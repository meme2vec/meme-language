"""
Text preprocessor for meme text
"""
from copy import deepcopy
import os
import re
import shutil
import string
import subprocess
import sys

from keras_preprocessing.sequence import pad_sequences
# import morfeusz2
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

if 'ipykernel' in sys.modules:
    from tqdm import tqdm_notebook as tqdm
else:
    from tqdm import tqdm


class TextPreprocessor:
    def __init__(self):
        self._stop_words = None
        self._word_exceptions = None

        self._load_all()

    def _load_all(self):
        self._load_word_exceptions()

    def _load_word_exceptions(self):
        exceptions = {
            'sie': 'się'
        }
        self._word_exceptions = exceptions

    def run(self, memes_df):
        df = memes_df.copy()

        original_txts = df['img_alt'].values
        lemmatized_txts = []

        for txt in tqdm(original_txts):
            lemmatized_txts.append(self._clean_text(txt))

        df['lemmatized_txt'] = lemmatized_txts
        df = df[df['lemmatized_txt'].map(lambda txt: len(txt) > 0)]
        return df

    def run_parallel(self, memes_df, workers):
        df = memes_df.copy()

        original_txts = df['img_alt'].values

        from multiprocessing import Pool

        with Pool(workers) as p:
            lemmatized_txts = list(tqdm(p.imap(self._clean_text, original_txts),
                                        total=len(original_txts)))

        df['lemmatized_txt'] = lemmatized_txts
        df = df[df['lemmatized_txt'].map(lambda txt: len(txt) > 0)]
        return df


# NOTE(pbielak): For now we want to rely only on WCRFT
# class MorfeuszTextPreprocessor(TextPreprocessor):
#    def __init__(self):
#        self._morf = morfeusz2.Morfeusz()
#
#    def _clean_text(self, text):
#        text = text.strip()
#        text = re.sub(r'[^a-zA-ZąęźłśćżóńĄĘŹŻŁŚĆÓŃ]', ' ', text)
#
#        lemmatized_words = []
#        for word in text.split():
#            if word in self._stop_words:
#                continue
#
#            if len(word) < 2:
#                continue
#
#            lemmatized_words.append(self._get_word_lemma(word))
#
#        return lemmatized_words
#
#    def _get_word_lemma(self, word):
#        if word in self._word_exceptions:
#            return self._word_exceptions[word]
#
#        lemmas = [candidate[2][1].split(':')[0].lower()
#                  for candidate in self._morf.analyse(word)]
#        return max(lemmas, key=lemmas.count)
#
#    def run_parallel(self, memes_df, workers):
#        raise RuntimeError('Not supported')


class WCRFTTextPreprocessor(TextPreprocessor):
    MEME_DIR = '/tmp/memes'
    WORD_REGEX = re.compile(r'[^a-zA-ZąęźłśćżóńĄĘŹŻŁŚĆÓŃ]')

    def __init__(self, progress_bar=True):
        super().__init__()
        self.progress_bar = progress_bar

    def run(self, memes_df):
        df = memes_df.copy()

        original_txts = df['img_alt'].values

        filenames = self._write_meme_texts_to_files(original_txts)
        run_tagger(os.path.join(self.MEME_DIR, 'full_list.txt'))
        lemmas, gram_categories = self._parse_tagged_texts(filenames)

        df['lemmatized_txt'] = lemmas
        df['grammatical_categories'] = gram_categories

        df = df[df['lemmatized_txt'].map(lambda txt: len(txt) > 0)]
        df = df[df['grammatical_categories'].map(lambda txt: len(txt) > 0)]

        shutil.rmtree(self.MEME_DIR)
        return df

    def _write_meme_texts_to_files(self, texts):
        os.makedirs(self.MEME_DIR)

        filenames = []
        for i, txt in tqdm(enumerate(texts),
                           total=len(texts), disable=not self.progress_bar):
            curr_filename = os.path.join(self.MEME_DIR, '%d.txt' % i)
            with open(curr_filename, 'w') as f:
                f.write(txt)
            filenames.append(curr_filename)

        with open(os.path.join(self.MEME_DIR, 'full_list.txt'), 'w') as f:
            f.write('\n'.join(filenames))

        return filenames

    def _parse_tagged_texts(self, filenames):
        results = []
        for filename in tqdm(filenames, disable=not self.progress_bar):
            lemmas, gram_cat = [], []

            with open(filename + '.tag', 'r') as f:
                tag_out = f.read()

            for line in tag_out.split('\n'):
                if not line:
                    continue

                base_word, lemma, tags = line.strip().split('\t')

                if len(base_word) < 2:
                    continue

                if self.WORD_REGEX.match(base_word):
                    continue

                tag = tags.split(':')[0] if ':' in tags else tags

                lemmas.append(lemma.lower())
                gram_cat.append(tag)

            results.append((lemmas, gram_cat))
        return list(zip(*results))


def run_tagger(filename):
    cmd = [
        'wcrft', 'nkjp_e2.ini',
        '-i', 'txt',
        '-o', 'iob-chan',
        '-d', '/usr/local/lib/python2.7/dist-packages/wcrft-1.0.0-py2.7.egg/wcrft/model/model_nkjp10_wcrft_e2/',
        '--batch',
        filename
    ]

    subprocess.run(' '.join(cmd), shell=True, check=True)


def get_words_mean_vector(dataset, text):
    vectors = [dataset.word_embeddings[word] for word in text if word > 0]
    if len(vectors) == 0:
        return np.ones(dataset.word_embeddings.shape[1])

    return np.mean(vectors, axis=0)


def make_mean_word_vectors(dataset):
    X_train = [get_words_mean_vector(dataset, text)
               for text in tqdm(dataset.X_train)]
    X_test = [get_words_mean_vector(dataset, text)
              for text in tqdm(dataset.X_test)]

    _ds = deepcopy(dataset)
    _ds._X_train = X_train
    _ds._X_test = X_test

    return _ds


def make_text_and_gram_cats_inputs(dataset, grammar_mappings=None):
    df = dataset._meme_texts

    unique_grammar_classes = set()
    df.grammatical_categories.apply(unique_grammar_classes.update)

    if not grammar_mappings:
        grammar_mappings = {gram: i + 1
                            for i, gram in enumerate(unique_grammar_classes)}

    def grammar_cats_to_sequence(gm_list):
        return [grammar_mappings[cat] for cat in gm_list]

    max_seq_len = dataset._max_text_emb_len

    grammar_sequences = df.grammatical_categories.map(grammar_cats_to_sequence)

    padded_grammar_seqs = pad_sequences(grammar_sequences, maxlen=max_seq_len)

    X = np.array([dataset._X, padded_grammar_seqs])
    Y = np.array(dataset._Y)

    X_train, X_test, Y_train, Y_test = train_test_split(
        np.arange(len(Y)), np.arange(len(Y)), test_size=0.2, random_state=42
    )

    X_train = list(X[:, X_train])
    Y_train = Y[Y_train]

    X_test = list(X[:, X_test])
    Y_test = Y[Y_test]

    _ds = deepcopy(dataset)
    _ds._X = X
    _ds._X_train = X_train
    _ds._Y_train = Y_train
    _ds._X_test = X_test
    _ds._Y_test = Y_test

    return _ds, grammar_mappings
