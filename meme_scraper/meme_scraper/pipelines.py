# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.exceptions import DropItem


class MemeScraperImagePipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        yield scrapy.Request('https:' + item['img_url'])

    def item_completed(self, results, item, info):
        image_ok, image_data = results[0]
        if not image_ok:
            raise DropItem('No image found')
        
        del item['img_url']
        item['img_file'] = os.path.split(image_data['path'])[1]
        return item
