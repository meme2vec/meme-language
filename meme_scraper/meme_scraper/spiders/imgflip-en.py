import re
from urllib import parse

import scrapy
from scrapy.loader.processors import Identity

from meme_scraper import utils
from meme_scraper import items


class ImgflipMemeLoader(utils.TakeFirstLoader):
    meme_text_regexp = re.compile(r'.*? \| (.*?) \|')

    @classmethod
    def _meme_text_processor(cls, texts):
        text = texts[0]
        m = cls.meme_text_regexp.match(text)
        if m:
            return m.group(1)
        return ''
    
    text_in = _meme_text_processor
    hashtags_out = Identity()
    comments_out = Identity()


class ImgflipCommentLoader(utils.TakeFirstLoader):
    @classmethod
    def _comment_upvotes_processor(cls, upvote_strings):
        upvote_string = upvote_strings[0]
        return int(upvote_string.split(' ')[0])

    upvotes_in = _comment_upvotes_processor


class ImgflipENSpider(scrapy.Spider):
    name = 'imgflip-en'
    start_urls = [
        'https://imgflip.com/',
    ]
    custom_settings = {
        'ITEM_PIPELINES': {
            'meme_scraper.pipelines.MemeScraperImagePipeline': 1
        },
        'IMAGES_STORE': './imgflip_images'
    }

    stats_regexp = re.compile(r'(.+) views, (.+) upvotes')

    def __init__(self, *args, max_page=10000, **kwargs):
        self.max_page = int(max_page)
        super().__init__(*args, **kwargs)

    def parse(self, response):
        # Get memes from current site
        meme_urls = response.css(
            'a.base-img-link'
        )
        
        for meme_url in meme_urls:
            yield response.follow(meme_url, self.parse_meme)

        #Proceed with next site
        next_site_link = response.css('a.pager-next')
        if next_site_link:
            yield response.follow(next_site_link[0], self.parse)
        else:
            parsed_url = list(parse.urlparse(response.url))
            current_page = int(parse.parse_qs(parsed_url[4])['page'][0])
            next_page = current_page + 1
            if next_page > self.max_page:
                return
            parsed_url[4] = parse.urlencode({'page': next_page})
            yield scrapy.Request(parse.urlunparse(parsed_url), self.parse)

    def parse_meme(self, response):
        loader = ImgflipMemeLoader(item=items.ImgflipENMemeItem(),
                                   response=response)

        loader.add_value('url', response.url)
        loader.add_css('img_url', 'img#im::attr(src)')

        loader.add_css('title', 'h1#img-title::text')
        loader.add_css('author', '.img-views .u-username::text')
        loader.add_css('text', 'img#im::attr(alt)')

        loader.add_css('hashtags', 'a.img-tag::text')

        views, upvotes = self._extract_views_and_upvotes_from_meme(response)
        loader.add_value('views', views)
        loader.add_value('upvotes', upvotes)

        loader.add_value('comments', list(self._parse_comment(response)))

        return loader.load_item()

    def _extract_views_and_upvotes_from_meme(self, response):
        stats_str = response.css('.img-views::text').extract_first()
        m = self.stats_regexp.match(stats_str)
        if m:
            stats = m.group(1), m.group(2)
            return [int(stat.replace(',', '')) for stat in stats]
        return None, None

    def _parse_comment(self, response):
        comments = response.css('div.com')

        for comment in comments:
            loader = ImgflipCommentLoader(item=items.ImgflipENCommItem(),
                                          selector=comment)

            loader.add_css('author', 'a.u-username::text')
            loader.add_css('upvotes', 'span.points::text')

            level_str = comment.attrib['class'][5:]
            level = int(level_str) if level_str else 0
            loader.add_value('level', level)

            yield loader.load_item()
